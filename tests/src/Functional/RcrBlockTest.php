<?php

namespace Drupal\Tests\rcr\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the rcr module.
 *
 * @group rcr
 */
class RcrBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'rcr',
  ];

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();

    \Drupal::state()->set('rcr.usd_russia', '99');
    \Drupal::state()->set('rcr.eur_russia', '102');

    $settings = [
      'country' => 'russia',
    ];

    $this->drupalPlaceBlock('rcr_rates', $settings);
  }

  /**
   * Tests that the block can be reached.
   */
  public function testBlock() {
    $this->drupalGet('<front>');
    $this->assertSession()->elementTextContains('xpath', "//p[1]/span[@class='rcr-curr-name']/text()", 'USD');
    $this->assertSession()->elementTextContains('xpath', "//p[1]/span[@class='rcr-curr-rate']/text()", '99 руб.');
    $this->assertSession()->elementTextContains('xpath', "//p[2]/span[@class='rcr-curr-name']/text()", 'EUR');
    $this->assertSession()->elementTextContains('xpath', "//p[2]/span[@class='rcr-curr-rate']/text()", '102 руб.');
  }

}
