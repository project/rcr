<?php

namespace Drupal\Tests\rcr\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests for the rcr module.
 *
 * @group rcr
 */
class RcrSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = [
    'rcr',
  ];

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $adminUser;

  /**
   * The User used for the test.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * Settings form url.
   */
  protected Url $settingsRoute;

  /**
   * SetUp the test class.
   */
  public function setUp(): void {
    parent::setUp();
    $this->settingsRoute = Url::fromRoute('rcr.currency_settings');
    $this->user = $this->DrupalCreateUser();
    $this->adminUser = $this->DrupalCreateUser([
      'change currency rate',
    ]);
  }

  /**
   * Tests that the settings page can be reached and saved.
   */
  public function testSettingsPage() {
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->user);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);
    $this->drupalGet($this->settingsRoute);
    $this->assertSession()->statusCodeEquals(200);

    $edit = [
      'currency_type' => '0',
      'country[russia]' => 'russia',
      'country[kyrgyzstan]' => 'kyrgyzstan',
      'update_type' => 'drush',
      'add_commission' => TRUE,
    ];

    $expected_values = [
      'currency_type' => '0',
      'country' => ['russia', 'kyrgyzstan'],
      'update_type' => 'drush',
      'add_commission' => TRUE,
    ];

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    foreach ($expected_values as $field => $expected_value) {
      $actual_value = $this->config('rcr.currency_settings')->get($field);
      $this->assertEquals($expected_value, $actual_value);
    }
  }

}
