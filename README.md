# CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

The module helps to get the exchange rate USD and EUR in relation to the Russian
ruble, Kazakhstan tenge, Kyrgyzstan som, Azerbaijan manat, Belarus ruble,
Ukrainian hryvnia.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/rcr>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/rcr>


## REQUIREMENTS

block (core)


## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit:
<https://www.drupal.org/node/1897420> for further information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configure > System > Currency settings
    3. Select country, update type, etc.
    4. Save configuration.
    5. Use cron or "drush rcr-getrates" to get rates.
    6. Add RCR block to any region if you need it.


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
