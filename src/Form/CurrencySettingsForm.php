<?php

namespace Drupal\rcr\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\rcr\RcrService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure currency settings.
 */
class CurrencySettingsForm extends ConfigFormBase {

  /**
   * Form constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory interface.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   State interface.
   * @param \Drupal\rcr\RcrService $rcrService
   *   RCR service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    protected readonly StateInterface $state,
    protected readonly RcrService $rcrService,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('state'),
      $container->get('rcr.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'rcr_currency_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['rcr.currency_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('rcr.currency_settings');

    $countries = $this->rcrService->getCountries();

    $form['country'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t("Select currency's country(s)"),
      '#description' => $this->t('Select at least one country'),
      '#options' => $countries,
      '#default_value' => $config->get('country') ?: 'russia',
      '#required' => TRUE,
    ];

    $form['update_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select currency update type'),
      '#options' => [
        'cron' => $this->t('Site cron'),
        'drush' => $this->t('Drush command (runs from server)'),
      ],
      '#default_value' => $config->get('update_type') ?: 'cron',
    ];

    $form['currency_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Get currency rates type'),
      '#options' => [
        0 => $this->t('Auto (get from banks)', [], ['context' => 'Currency update type']),
        1 => $this->t('Manual', [], ['context' => 'Currency update type']),
      ],
      '#default_value' => $config->get('currency_type'),
    ];

    foreach ($countries as $key => $name) {
      $form["usd_manual_$key"] = [
        '#type' => 'number',
        '#title' => $this->t("Manual @name to USD currency rate", ['@name' => $name]),
        '#min' => 0,
        '#step' => 0.01,
        '#default_value' => $this->state->get("rcr.usd_manual_$key") ?: '',
        '#states' => [
          'visible' => [
            ':input[name="currency_type"]' => ['value' => '1'],
            ':input[name="country[' . $key . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    $form['commission'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Add commission'),
    ];

    $form['commission']['add_commission'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add commission to rates'),
      '#default_value' => $config->get('add_commission'),
    ];

    foreach ($countries as $key => $name) {
      $form['commission']["usd_commission_$key"] = [
        '#type' => 'number',
        '#title' => $this->t("Commission value @name to USD currency rate (%)", ['@name' => $name]),
        '#default_value' => $this->state->get("rcr.usd_commission_$key") ?: 0,
        '#min' => 0,
        '#max' => 100,
        '#step' => 0.01,
        '#states' => [
          'visible' => [
            ':input[name="add_commission"]' => ['checked' => TRUE],
            ':input[name="country[' . $key . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $this->config('rcr.currency_settings')
      ->set('country', array_keys(array_filter($values['country'])))
      ->set('currency_type', $values['currency_type'])
      ->set('update_type', $values['update_type'])
      ->set('add_commission', $values['add_commission'])
      ->save();

    $countries = $this->rcrService->getCountries();
    foreach ($countries as $key => $name) {
      $this->state->set("rcr.usd_manual_$key", $values["usd_manual_$key"]);
      $this->state->set("rcr.usd_commission_$key", $values["usd_commission_$key"]);
    }

    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

}
