<?php

namespace Drupal\rcr\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\State\StateInterface;
use Drupal\rcr\RcrService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a currencies rates block.
 *
 * @Block(
 *   id = "rcr_rates",
 *   admin_label = @Translation("Currencies rates"),
 * )
 */
class RatesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new RatesBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state store.
   * @param \Drupal\rcr\RcrService $rcrService
   *   RCR service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly StateInterface $state,
    protected readonly RcrService $rcrService,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('rcr.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'country' => 'russia',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['country'] = [
      '#type' => 'radios',
      '#title' => $this->t("Select currency's country"),
      '#description' => $this->t('Select at the country'),
      '#options' => $this->rcrService->getCountries(),
      '#default_value' => $this->configuration['country'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['country'] = $values['country'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('rcr.currency_settings');
    $country = $this->configuration['country'];
    $symbol = '';

    switch ($country) {
      case 'russia':
      case 'belarus':
        $symbol = 'руб.';
        break;

      case 'kazakhstan':
        $symbol = 'тңг.';
        break;

      case 'kyrgyzstan':
        $symbol = 'сом';
        break;

      case 'azerbaijan':
        $symbol = 'ман.';
        break;

      case 'ukraine':
        $symbol = 'грн.';
        break;
    }

    $update_type = $config->get('currency_type');
    $usd_rate = 0;
    // Auto.
    if ($update_type === '0') {
      $usd_rate = $this->state->get("rcr.usd_$country");
    }
    // Manual.
    elseif ($update_type === '1') {
      $usd_rate = $this->state->get("rcr.usd_manual_$country");
    }

    $build = [
      '#theme' => 'rcr_block',
      '#usd' => $usd_rate,
      '#eur' => $this->state->get("rcr.eur_$country"),
      '#symbol' => $symbol,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
