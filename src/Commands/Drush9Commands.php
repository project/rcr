<?php

namespace Drupal\rcr\Commands;

use Drupal\rcr\RcrService;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 */
class Drush9Commands extends DrushCommands {

  /**
   * Drush command constructor.
   *
   * @param \Drupal\rcr\RcrService $rcrService
   *   RCR service.
   */
  public function __construct(
    protected readonly RcrService $rcrService,
  ) {}

  /**
   * Get current rates.
   *
   * @command rcr:getrates
   * @aliases rcr-getrates
   * @usage rcr:getrates
   */
  public function getrates() {
    $message = $this->rcrService->getRates();
    $this->output()->writeln($message);
  }

}
