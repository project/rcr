<?php

namespace Drupal\rcr;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * RCR service.
 */
class RcrService {

  use StringTranslationTrait;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state store.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly StateInterface $state,
  ) {}

  /**
   * Create function.
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('state'),
    );
  }

  /**
   * Get rates for Russia.
   */
  public function getRatesRussia(): string {
    $message = '';
    $nowStr = date('d.m.Y');
    $dataxml = file_get_contents('https://www.cbr.ru/scripts/XML_daily.asp?date_req=' . $nowStr);
    if ($dataxml !== FALSE) {
      $xml = new \SimpleXMLElement($dataxml);
      if (!empty($xml->Valute)) {
        foreach ($xml->Valute as $v) {
          if ((string) $v->CharCode === 'USD') {
            $replace_comma = str_replace(',', '.', $v->Value);
            $this->state->set('rcr.usd_russia', round($replace_comma, 2));
            $message = 'Russia USD rate was saved. ';
          }
          if ((string) $v->CharCode === 'EUR') {
            $replace_comma = str_replace(',', '.', $v->Value);
            $this->state->set('rcr.eur_russia', round($replace_comma, 2));
            $message .= 'Russia EUR rate was saved. ';
          }
        }
      }
      return $message;
    }

    return 'Can not get current rates from www.cbr.ru';
  }

  /**
   * Get rates for Kazakhstan.
   */
  public function getRatesKazakhstan(): string {
    $message = '';
    $dataxml = file_get_contents('https://www.nationalbank.kz/rss/rates_all.xml');
    if ($dataxml !== FALSE) {
      $xml = new \SimpleXMLElement($dataxml);
      $channel = $xml->channel;
      if ($channel !== NULL) {
        foreach ($channel->item as $v) {
          if ((string) $v->title === 'USD') {
            $this->state->set('rcr.usd_kazakhstan', (string) $v->description);
            $message = 'Kazakhstan USD rate was saved. ';
          }
          if ((string) $v->title === 'EUR') {
            $this->state->set('rcr.eur_kazakhstan', (string) $v->description);
            $message .= 'Kazakhstan EUR rate was saved. ';
          }
        }
      }
      return $message;
    }

    return 'Can not get current rates from www.nationalbank.kz';
  }

  /**
   * Get rates for Kyrgyzstan.
   */
  public function getRatesKyrgyzstan(): string {
    $message = '';
    $dataxml = file_get_contents('https://www.nbkr.kg/XML/daily.xml');
    if ($dataxml !== FALSE) {
      $xml = new \SimpleXMLElement($dataxml);
      foreach ($xml->Currency as $v) {
        if ($v->attributes()->ISOCode->__toString() === 'USD') {
          $replace_comma = str_replace(',', '.', $v->Value->__toString());
          $this->state->set('rcr.usd_kyrgyzstan', round($replace_comma, 2));
          $message = 'Kyrgyzstan USD rate was saved. ';
        }
        if ($v->attributes()->ISOCode->__toString() === 'EUR') {
          $replace_comma = str_replace(',', '.', $v->Value->__toString());
          $this->state->set('rcr.eur_kyrgyzstan', round($replace_comma, 2));
          $message .= 'Kyrgyzstan EUR rate was saved. ';
        }
      }
      return $message;
    }

    return 'Can not get current rates from www.nbkr.kg';
  }

  /**
   * Get rates for Azerbaijan.
   */
  public function getRatesAzerbaijan(): string {
    $message = '';
    $nowStr = date('d.m.Y');
    $dataxml = file_get_contents('https://www.cbar.az/currencies/' . $nowStr . '.xml');
    if ($dataxml !== FALSE) {
      $xml = new \SimpleXMLElement($dataxml);
      if (!empty($xml->ValType)) {
        $valutes = $xml->xpath('ValType[@Type="Xarici valyutalar"]')[0];
        foreach ($valutes->Valute as $v) {
          if ((string) $v->attributes()->Code === 'USD') {
            $this->state->set('rcr.usd_azerbaijan', round((string) $v->Value, 2));
            $message = 'Azerbaijan USD rate was saved. ';
          }
          if ((string) $v->attributes()->Code === 'EUR') {
            $this->state->set('rcr.eur_azerbaijan', round((string) $v->Value, 2));
            $message .= 'Azerbaijan EUR rate was saved. ';
          }
        }
      }
      return $message;
    }

    return 'Can not get current rates from www.cbar.az';
  }

  /**
   * Get rates for Belarus.
   */
  public function getRatesBelarus(): string {
    $message = '';
    $nowStr = date('m/d/Y');
    $dataxml = file_get_contents('https://services.nbrb.by/xmlexrates.aspx?ondate=' . $nowStr);
    if ($dataxml !== FALSE) {
      $xml = new \SimpleXMLElement($dataxml);
      if (!empty($xml->Currency)) {
        foreach ($xml->Currency as $c) {
          if ((string) $c->CharCode === 'USD') {
            $this->state->set('rcr.usd_belarus', round((float) $c->Rate, 2));
            $message = 'Belarus USD rate was saved. ';
          }
          if ((string) $c->CharCode === 'EUR') {
            $this->state->set('rcr.eur_belarus', round((float) $c->Rate, 2));
            $message .= 'Belarus EUR rate was saved. ';
          }
        }
      }
      return $message;
    }

    return 'Can not get current rates from www.nbrb.by';
  }

  /**
   * Get rates for Ukraine.
   */
  public function getRatesUkraine(): string {
    $message = '';
    $nowStr = date('Ymd');
    $dataxml = file_get_contents('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=' . $nowStr);
    if ($dataxml !== FALSE) {
      $xml = new \SimpleXMLElement($dataxml);
      if (!empty($xml->currency)) {
        foreach ($xml->currency as $c) {
          if ((string) $c->cc === 'USD') {
            $this->state->set('rcr.usd_ukraine', round((float) $c->rate, 2));
            $message = 'Ukraine USD rate was saved. ';
          }
          if ((string) $c->cc === 'EUR') {
            $this->state->set('rcr.eur_ukraine', round((float) $c->rate, 2));
            $message .= 'Ukraine EUR rate was saved. ';
          }
        }
      }
      return $message;
    }

    return 'Can not get current rates from www.cbr.ru';
  }

  /**
   * Get rates from banks.
   */
  public function getRates(): string {
    $config = $this->configFactory->get('rcr.currency_settings');
    $countries = $config->get('country');
    $message = '';

    foreach ($countries as $country) {
      switch ($country) {
        case 'russia':
          $message .= $this->getRatesRussia();
          break;

        case 'kazakhstan':
          $message .= $this->getRatesKazakhstan();
          break;

        case 'kyrgyzstan':
          $message .= $this->getRatesKyrgyzstan();
          break;

        case 'azerbaijan':
          $message .= $this->getRatesAzerbaijan();
          break;

        case 'belarus':
          $message .= $this->getRatesBelarus();
          break;

        case 'ukraine':
          $message .= $this->getRatesUkraine();
          break;
      }
    }

    return $message;
  }

  /**
   * Get countries list.
   */
  public function getCountries() {
    return [
      'russia' => $this->t('Russia'),
      'kazakhstan' => $this->t('Kazakhstan'),
      'kyrgyzstan' => $this->t('Kyrgyzstan'),
      'azerbaijan' => $this->t('Azerbaijan'),
      'belarus' => $this->t('Belarus'),
      'ukraine' => $this->t('Ukraine'),
    ];
  }

}
